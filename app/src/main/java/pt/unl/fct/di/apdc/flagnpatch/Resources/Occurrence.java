package pt.unl.fct.di.apdc.flagnpatch.Resources;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public class Occurrence implements Parcelable {
    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Occurrence> CREATOR = new Parcelable.Creator<Occurrence>() {
        @Override
        public Occurrence createFromParcel(Parcel in) {
            return new Occurrence(in);
        }

        @Override
        public Occurrence[] newArray(int size) {
            return new Occurrence[size];
        }
    };
    public boolean followed;
    public ArrayList<OccComments> comments;
    String type;
    String description;
    String imageURL;
    String address;
    String district;
    String county;
    String id;
    String state;
    int priority;
    double latitude;
    double longitude;
    String dateOfReport;
    String followers;

    public Occurrence(String type, String description, String imageURL, String address,
                      String district, String county, String id, int priority, String state,
                      double latitude, double longitude, String dateOfReport, String followers, boolean followed) {
        this.type = type;
        this.description = description;
        this.imageURL = imageURL;
        this.address = address;
        this.district = district;
        this.county = county;
        this.state = state;
        this.id = id;
        this.priority = priority;
        this.latitude = latitude;
        this.longitude = longitude;
        this.dateOfReport = dateOfReport;
        this.followers = followers;
        comments = new ArrayList<>();
        this.followed = followed;
    }

    protected Occurrence(Parcel in) {
        type = in.readString();
        description = in.readString();
        imageURL = in.readString();
        address = in.readString();
        district = in.readString();
        county = in.readString();
        id = in.readString();
        state = in.readString();
        priority = in.readInt();
        latitude = in.readDouble();
        longitude = in.readDouble();
        dateOfReport = in.readString();
        followers = in.readString();
        followed = in.readByte() != 0x00;
        if (in.readByte() == 0x01) {
            comments = new ArrayList<OccComments>();
            in.readList(comments, OccComments.class.getClassLoader());
        } else {
            comments = null;
        }
    }

    public String getType() {

        return type;
    }

    public String getDescription() {

        return description;
    }

    public String getImageURL() {

        return imageURL;
    }

    public String getAddress() {

        return address;
    }

    public String getDistrict() {

        return district;
    }

    public String getCounty() {

        return county;
    }

    public String getId() {

        return id;
    }

    public String getState() {

        return state;
    }

    public int getPriority() {

        return priority;
    }

    public double getLatitude() {

        return latitude;
    }

    public double getLongitude() {

        return longitude;
    }

    public String getCreationDate() {

        return dateOfReport;
    }

    public String getFollowers() {

        return followers;
    }

    public void changeComments(OccComments occ) {

        comments.add(occ);
    }

    public void addComments(ArrayList<OccComments> comments) {

        if (comments != null)
            this.comments = comments;
    }

    public ArrayList<OccComments> getComments() {

        return comments;
    }

    public boolean getFollowed() {
        return followed;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(description);
        dest.writeString(imageURL);
        dest.writeString(address);
        dest.writeString(district);
        dest.writeString(county);
        dest.writeString(id);
        dest.writeString(state);
        dest.writeInt(priority);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(dateOfReport);
        dest.writeString(followers);
        dest.writeByte((byte) (followed ? 0x01 : 0x00));
        if (comments == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(comments);
        }
    }
}
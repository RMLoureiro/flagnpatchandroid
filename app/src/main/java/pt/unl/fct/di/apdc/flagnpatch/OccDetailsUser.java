package pt.unl.fct.di.apdc.flagnpatch;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import pt.unl.fct.di.apdc.flagnpatch.Database.DBHandler;
import pt.unl.fct.di.apdc.flagnpatch.Resources.General.TokenData;
import pt.unl.fct.di.apdc.flagnpatch.Resources.OccComments;
import pt.unl.fct.di.apdc.flagnpatch.Resources.Occurrence;
import pt.unl.fct.di.apdc.flagnpatch.Utils.AppSingleton;


public class OccDetailsUser extends AppCompatActivity {
    private static final String TAG = "ChangeState";
    private static final String NO_PHOTO = "/img/missing.png";
    private static final String DEFAULT_PHOTO = "https://itviec.com/assets/missing.png";

    int currentState, selected;
    AppSingleton appSingleton;
    private Occurrence occ;
    private DBHandler datastore;
    private ImageView image;
    private Bundle bundle;
    private int newPriority;
    private TokenData token;
    private Button follow;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.occ_details_user);

        bundle = getIntent().getBundleExtra(string(R.string.bundle));
        occ = bundle.getParcelable(string(R.string.o_occ));
        System.out.println(occ.followed + " " + occ.getFollowed());

        datastore = new DBHandler(this);
        appSingleton = AppSingleton.getInstance(this);
        token = datastore.getToken();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        image = (ImageView) findViewById(R.id.image_user);
        image.setFocusableInTouchMode(true);
        TextView type = (TextView) findViewById(R.id.occ_type_user);
        TextView priority = (TextView) findViewById(R.id.occ_priority_detail_user);
        TextView address = (TextView) findViewById(R.id.occ_address_detail_user);
        TextView description = (TextView) findViewById(R.id.occ_description_detail_user);
        TextView state = (TextView) findViewById(R.id.occ_state_detail_user);
        follow = (Button) findViewById(R.id.follow_occ);

        System.out.println(occ.followed + " " + occ.getFollowed());

        if (!occ.getFollowed()) {
            follow.setBackgroundColor(Color.parseColor("#0099FF"));
            follow.setText(string(R.string.o_follow));
        } else {
            follow.setBackgroundColor(Color.parseColor("#FF1A1A"));
            follow.setText(string(R.string.o_unfollow));
        }


        follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!occ.getFollowed()) {
                    AlertDialog.Builder b = new AlertDialog.Builder(new ContextThemeWrapper(OccDetailsUser.this, R.style.AppTheme_Dark));
                    b.setTitle(string(R.string.o_sugestion_priority));
                    String[] types = {"1", "2", "3", "4", "5"};
                    b.setItems(types, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            newPriority = which + 1;

                            JSONObject request = new JSONObject();
                            JSONObject tokenJson = new JSONObject();
                            try {
                                tokenJson.accumulate(string(R.string.t_user), token.getUser());
                                tokenJson.accumulate(string(R.string.t_id), token.getId());
                                tokenJson.accumulate(string(R.string.t_creation), token.getCreationDate());
                                tokenJson.accumulate(string(R.string.t_expiration), token.getExpirationDate());
                                tokenJson.accumulate(string(R.string.t_role), token.getRole());

                                request.accumulate(string(R.string.j_token), tokenJson);
                                request.accumulate(string(R.string.j_priority), newPriority);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, string(R.string.ser_follow) + occ.getId()
                                    , request, new Response.Listener<JSONObject>() {

                                @Override
                                public void onResponse(JSONObject response) {
                                    follow.setBackgroundColor(Color.parseColor("#FF1A1A"));
                                    follow.setText(string(R.string.o_unfollow));
                                    appSingleton.myReports.remove(occ);
                                    occ.followed = true;
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    if (error == null || error.networkResponse == null) {
                                        return;
                                    }
                                }
                            });
                            appSingleton.addToRequestQueue(jsObjRequest, TAG);

                        }

                    });
                    b.show();


                } else {

                    JSONObject tokenJson = new JSONObject();
                    try {
                        tokenJson.accumulate(string(R.string.t_user), token.getUser());
                        tokenJson.accumulate(string(R.string.t_id), token.getId());
                        tokenJson.accumulate(string(R.string.t_creation), token.getCreationDate());
                        tokenJson.accumulate(string(R.string.t_expiration), token.getExpirationDate());
                        tokenJson.accumulate(string(R.string.t_role), token.getRole());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, string(R.string.ser_unfollow) + occ.getId()
                            , tokenJson, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            follow.setBackgroundColor(Color.parseColor("#0099FF"));
                            follow.setText(string(R.string.o_follow));
                            appSingleton.myReports.remove(occ);
                            occ.followed = false;
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error == null || error.networkResponse == null) {
                                return;
                            }
                        }
                    });
                    appSingleton.addToRequestQueue(jsObjRequest, TAG);
                }
            }
        });
        if (occ.getImageURL().equals(NO_PHOTO) ||
                occ.getImageURL().equals(DEFAULT_PHOTO)) {
            image.setImageResource(R.mipmap.missing);
            alreadyChoosed();
            image.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            ImageRequest imageRequest = new ImageRequest(
                    occ.getImageURL(), // Image URL
                    new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap response) {
                            image.setImageBitmap(response);
                            alreadyChoosed();
                        }
                    },
                    0, // Image width
                    0, // Image height
                    ImageView.ScaleType.CENTER_CROP, // Image scale type
                    Bitmap.Config.RGB_565, //Image decode configuration
                    new Response.ErrorListener() { // Error listener
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // Do something with error response
                            if (error == null || error.networkResponse == null) {
                                return;
                            }
                        }
                    }
            );
            appSingleton.addToRequestQueue(imageRequest, TAG);
        }

        final String[] types = {string(R.string.st_pendente), string(R.string.st_em_resolucao),
                string(R.string.st_resolvido), string(R.string.st_rejeitado)};


        type.setText(string(R.string.o_type2) + occ.getType());
        priority.setText(string(R.string.o_priority2) + occ.getPriority());
        address.setText(string(R.string.o_address2) + occ.getAddress());
        description.setText(string(R.string.o_description2) + occ.getDescription());
        state.setText(string(R.string.o_status) + occ.getState());

        for (int i = 0; i < types.length; i++) {
            if (types[i].equals(occ.getState())) {
                currentState = selected = i;
                break;
            }
        }


        CommentsFragment comments = new CommentsFragment();
        Bundle args = new Bundle();
        args.putString(string(R.string.t_id), occ.getId());
        comments.setArguments(args);
        getSupportFragmentManager().beginTransaction().add(R.id.details_user, comments).commit();

        Button comment = (Button) findViewById(R.id.comment);
        comment.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText comment = (EditText) findViewById(R.id.content);
                final String commentContent = comment.getText().toString();

                JSONObject request = new JSONObject();
                JSONObject content = new JSONObject();
                JSONObject tokenJson = new JSONObject();


                try {
                    tokenJson.accumulate(string(R.string.t_user), token.getUser());
                    tokenJson.accumulate(string(R.string.t_id), token.getId());
                    tokenJson.accumulate(string(R.string.t_creation), token.getCreationDate());
                    tokenJson.accumulate(string(R.string.t_expiration), token.getExpirationDate());
                    tokenJson.accumulate(string(R.string.t_role), token.getRole());

                    content.accumulate(string(R.string.c_content), commentContent);

                    request.accumulate(string(R.string.c_comment), content);
                    request.accumulate(string(R.string.j_token), tokenJson);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, string(R.string.ser_add_comment) + occ.getId(), request, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(final JSONObject response) {
                        comment.setText("");
                        LayoutInflater inflater = getLayoutInflater();


                        LinearLayout layout = (LinearLayout) findViewById(R.id.comment_list);
                        ViewGroup container = (ViewGroup) findViewById(R.id.comment_list);
                        View commentView = inflater.inflate(R.layout.comment_item_list, container, false);

                        OccComments comment = null;
                        try {
                            comment = new OccComments(response.getString(string(R.string.c_content)),
                                    response.getString(string(R.string.c_author_email)),
                                    response.getString(string(R.string.c_author_name)),
                                    response.getInt(string(R.string.c_author_identifier)),
                                    response.getInt(string(R.string.c_register_time)),
                                    response.getString(string(R.string.c_register_date)));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        TextView description = (TextView) commentView.findViewById(R.id.description);
                        TextView name = (TextView) commentView.findViewById(R.id.name);
                        TextView authorId = (TextView) commentView.findViewById(R.id.author_id_detail);

                        authorId.setText("(" + comment.authorId + ")");
                        description.setText(comment.content);
                        name.setText(comment.authorName);

                        layout.addView(commentView);
                    }
                },
                        new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if (error == null || error.networkResponse == null) {
                                    return;
                                }
                                if (error.networkResponse.statusCode == 401) {
                                    tokenLost();
                                } else
                                    Toast.makeText(getBaseContext(), R.string.e_server_down, Toast.LENGTH_LONG).show();

                            }
                        });
                appSingleton.addToRequestQueue(jsObjRequest, TAG);
            }
        });


    }

    private String string(int string) {
        return this.getResources().getString(string);
    }


    private void tokenLost() {
        Toast.makeText(this, string(R.string.e_expired_session), Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    private void alreadyChoosed() {
        int color = getResources().getColor(R.color.primary);
//        findViewById(R.id.stroke).setBackgroundColor(color);
        image.setBackgroundColor(color);
        image.setScaleType(ImageView.ScaleType.FIT_CENTER);
        image.setEnabled(false);
    }
}


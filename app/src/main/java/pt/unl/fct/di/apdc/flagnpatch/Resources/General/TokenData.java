package pt.unl.fct.di.apdc.flagnpatch.Resources.General;


public class TokenData {

    private String user;
    private String id;
    private long creationDate;
    private long expirationDate;
    private String role;

    public TokenData(String user, String id, long creationDate, long expirationDate, String role) {
        this.user = user;
        this.id = id;
        this.creationDate = creationDate;
        this.expirationDate = expirationDate;
        this.role = role;
    }

    public String getUser() {
        return user;
    }

    public String getId() {
        return id;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public long getExpirationDate() {
        return expirationDate;
    }

    public String getRole() {
        return role;
    }
}

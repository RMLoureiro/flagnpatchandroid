package pt.unl.fct.di.apdc.flagnpatch;


import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import pt.unl.fct.di.apdc.flagnpatch.Database.DBHandler;
import pt.unl.fct.di.apdc.flagnpatch.Resources.General.TokenData;
import pt.unl.fct.di.apdc.flagnpatch.Resources.Occurrence;
import pt.unl.fct.di.apdc.flagnpatch.Utils.AppSingleton;


public class OccDetailsWorker extends AppCompatActivity {
    private static final String TAG = "ChangeState";
    private static final String NO_PHOTO = "/img/missing.png";
    private static final String DEFAULT_PHOTO = "https://itviec.com/assets/missing.png";

    int currentState, selected;
    AppSingleton appSingleton;
    private Occurrence occ;
    private ImageView image;
    private DBHandler datastore;
    private Bundle bundle;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        datastore = new DBHandler(this);
        setContentView(R.layout.occ_details);
        final Button saveChanges = (Button) findViewById(R.id.save_changes);
        appSingleton = AppSingleton.getInstance(this);
        bundle = getIntent().getBundleExtra(string(R.string.bundle));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        image = (ImageView) findViewById(R.id.image_worker);
        image.setFocusableInTouchMode(true);
        TextView tipo = (TextView) findViewById(R.id.occ_type);
        TextView priority = (TextView) findViewById(R.id.occ_priority_detail);
        TextView address = (TextView) findViewById(R.id.occ_address_detail);
        TextView description = (TextView) findViewById(R.id.occ_description_detail);
        final Spinner state = (Spinner) findViewById(R.id.occ_state_detail);

        occ = bundle.getParcelable(string(R.string.o_occ));

        if (occ.getImageURL().equals(NO_PHOTO) ||
                occ.getImageURL().equals(DEFAULT_PHOTO)) {
            image.setImageResource(R.mipmap.missing);
            alreadyChoosed();
            image.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            ImageRequest imageRequest = new ImageRequest(
                    occ.getImageURL(), // Image URL
                    new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap response) {
                            image.setImageBitmap(response);
                            alreadyChoosed();
                        }
                    },
                    0, // Image width
                    0, // Image height
                    ImageView.ScaleType.CENTER_CROP, // Image scale type
                    Bitmap.Config.RGB_565, //Image decode configuration
                    new Response.ErrorListener() { // Error listener
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // Do something with error response
                            if (error == null || error.networkResponse == null) {
                                return;
                            }
                        }
                    }
            );
            appSingleton.addToRequestQueue(imageRequest, TAG);
        }

        final String[] types2 = {string(R.string.st_pendente), string(R.string.st_em_resolucao), string(R.string.st_resolvido), string(R.string.st_rejeitado)};


        tipo.setText(string(R.string.o_type2) + occ.getType());
        priority.setText(string(R.string.o_priority2) + occ.getPriority());
        address.setText(string(R.string.o_address2) + occ.getAddress());
        description.setText(string(R.string.o_description2) + occ.getDescription());

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                    android.R.layout.simple_spinner_dropdown_item, types2);

            for (int i = 0; i < types2.length; i++) {
                if (types2[i].equals(occ.getState())) {
                    currentState = selected = i;
                    break;
                }
            }

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            state.setAdapter(adapter);

            state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (currentState != position) {
                        saveChanges.setVisibility(View.VISIBLE);
                    } else {
                        saveChanges.setVisibility(View.GONE);
                    }
                    selected = position;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    return;
                }

            });


            saveChanges.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(OccDetailsWorker.this, R.style.AppTheme_Dark));
                    builder.setTitle(string(R.string.o_change_status_description));

                    // Set up the input
                    final EditText input = new EditText(getApplicationContext());
                    // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                    input.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
                    input.setSingleLine(false);
                    input.setLines(4);
                    input.setMaxLines(6);
                    input.setGravity(Gravity.LEFT | Gravity.TOP);
                    input.setHorizontalScrollBarEnabled(true);
                    input.setPadding(100, 50, 100, 100);
                    builder.setView(input);

                    // Set up the buttons
                    builder.setPositiveButton(string(R.string.w_save), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            changeState(types2, saveChanges, input.getText().toString());
                        }
                    });
                    builder.setNegativeButton(string(R.string.o_exit), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();
                }
            });


        CommentsFragment comments = new CommentsFragment();
        Bundle args = new Bundle();
        args.putString(string(R.string.t_id), occ.getId());
        comments.setArguments(args);

        getSupportFragmentManager().beginTransaction().add(R.id.details, comments).commit();
    }

    private void alreadyChoosed() {
        int color = getResources().getColor(R.color.primary);
//        findViewById(R.id.stroke).setBackgroundColor(color);
        image.setBackgroundColor(color);
        image.setScaleType(ImageView.ScaleType.FIT_CENTER);
        image.setEnabled(false);
    }

    private void tokenLost() {
        Toast.makeText(this, string(R.string.e_expired_session), Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    private void changeState(String[] types, final Button saveChanges, String comment) {
        String url = string(R.string.ser_change_state) + occ.getId();
        TokenData token = datastore.getToken();
        JSONObject request = new JSONObject();
        JSONObject tokenJson = new JSONObject();
        JSONObject status = new JSONObject();

        try {
            tokenJson.accumulate(string(R.string.t_user), token.getUser());
            tokenJson.accumulate(string(R.string.t_id), token.getId());
            tokenJson.accumulate(string(R.string.t_creation), token.getCreationDate());
            tokenJson.accumulate(string(R.string.t_expiration), token.getExpirationDate());
            tokenJson.accumulate(string(R.string.t_role), token.getRole());

            status.accumulate(string(R.string.j_state), types[selected]);
            status.accumulate(string(R.string.j_description), comment);

            request.accumulate(string(R.string.j_token), tokenJson);
            request.accumulate(string(R.string.j_status), status);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, url, request, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(final JSONObject response) {
                currentState = selected;
                saveChanges.setVisibility(View.GONE);
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error == null || error.networkResponse == null) {
                            return;
                        }
                        Toast.makeText(getBaseContext(), R.string.e_server_down, Toast.LENGTH_LONG).show();

                    }
                });
        appSingleton.addToRequestQueue(jsObjRequest, TAG);
    }

    private String string(int string) {
        return this.getResources().getString(string);
    }

}

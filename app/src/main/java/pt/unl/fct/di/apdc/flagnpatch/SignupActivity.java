package pt.unl.fct.di.apdc.flagnpatch;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import butterknife.Bind;
import butterknife.ButterKnife;
import pt.unl.fct.di.apdc.flagnpatch.Utils.AppSingleton;

public class SignupActivity extends AppCompatActivity {
    private static final String TAG = "SignupActivity";
    @Bind(R.id.input_firstname)
    EditText _nameText;
    @Bind(R.id.input_lastname)
    EditText _lastnameText;
    @Bind(R.id.input_email)
    EditText _emailText;
    @Bind(R.id.input_district)
    Spinner _districtText;
    @Bind(R.id.input_county)
    Spinner _countyText;
    @Bind(R.id.input_password)
    EditText _passwordText;
    @Bind(R.id.input_reEnterPassword)
    EditText _reEnterPasswordText;
    @Bind(R.id.btn_signup)
    Button _signupButton;
    @Bind(R.id.link_login)
    TextView _loginLink;
    private AppSingleton appSingleton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appSingleton = AppSingleton.getInstance(this);

        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLogin();
            }
        });

        _countyText.setEnabled(false);

        final ArrayList<String> districtOptions = new ArrayList<String>();
        final ArrayList<String> countyOptions = new ArrayList<String>();
        districtOptions.add(string(R.string.e_district));

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, string(R.string.ser_address), null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(final JSONObject response) {
                        if (response != null) {
                            Iterator it = response.keys();
                            while (it.hasNext()) {
                                districtOptions.add(it.next().toString());
                            }
                            ArrayAdapter<String> districtAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                    android.R.layout.simple_spinner_dropdown_item, districtOptions) {
                                @Override
                                public boolean isEnabled(int position) {
                                    return position != 0;
                                }

                                @Override
                                public View getDropDownView(int position, View convertView,
                                                            ViewGroup parent) {
                                    View view = super.getDropDownView(position, convertView, parent);
                                    TextView tv = (TextView) view;
                                    if (position == 0)
                                        tv.setTextColor(Color.GRAY);
                                    else
                                        tv.setTextColor(Color.WHITE);

                                    return view;
                                }
                            };
                            _districtText.setAdapter(districtAdapter);

                            _districtText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    countyOptions.clear();
                                    countyOptions.add(string(R.string.e_county));
                                    _countyText.setEnabled(true);
                                    if (position > 0) {
                                        try {
                                            JSONArray array = (JSONArray) response.get(districtOptions.get(position));
                                            for (int i = 0; i < array.length(); i++) {
                                                countyOptions.add(array.getString(i));
                                            }
                                            ArrayAdapter<String> countyAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                                    android.R.layout.simple_spinner_dropdown_item, countyOptions) {
                                                @Override
                                                public boolean isEnabled(int position) {
                                                    return position != 0;
                                                }

                                                @Override
                                                public View getDropDownView(int position, View convertView,
                                                                            ViewGroup parent) {
                                                    View view = super.getDropDownView(position, convertView, parent);
                                                    TextView tv = (TextView) view;
                                                    if (position == 0)
                                                        tv.setTextColor(Color.GRAY);
                                                    else
                                                        tv.setTextColor(Color.WHITE);

                                                    return view;
                                                }
                                            };
                                            _countyText.setAdapter(countyAdapter);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    return;
                                }
                            });
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error == null || error.networkResponse == null) {
                            return;
                        }
                        Toast.makeText(getBaseContext(), string(R.string.e_server_down), Toast.LENGTH_LONG).show();

                    }
                });

        appSingleton.addToRequestQueue(jsObjRequest, TAG);

    }

    public void signup() {
        Log.d(TAG, "Signup");

        if (!validate()) {
            onSignupFailed();
            return;
        }

        _signupButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(string(R.string.s_creating_acc));
        progressDialog.show();

        String name = _nameText.getText().toString();
        String lastname = _lastnameText.getText().toString();
        String email = _emailText.getText().toString();
        String district = _districtText.getSelectedItem().toString();
        String county = _countyText.getSelectedItem().toString();
        String password = _passwordText.getText().toString();
        String password_confirm = _reEnterPasswordText.getText().toString();


        try {
            final JSONObject credentials = new JSONObject();
            credentials.accumulate(string(R.string.j_email), email);
            credentials.accumulate(string(R.string.j_email_confirm), email);
            credentials.accumulate(string(R.string.j_pwd), password);
            credentials.accumulate(string(R.string.j_pwd_confirm), password_confirm);
            credentials.accumulate(string(R.string.j_name), name + " " + lastname);
            JSONObject address = new JSONObject();
            address.accumulate(string(R.string.j_district), district);
            address.accumulate(string(R.string.j_county), county);
            credentials.accumulate(string(R.string.j_address), address);


            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.POST, string(R.string.ser_signup), credentials, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {

                            new Handler().postDelayed(
                                    new Runnable() {
                                        public void run() {
                                            onSignupSuccess();
                                            progressDialog.dismiss();
                                        }
                                    }, 3000);
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error == null || error.networkResponse == null) {
                                return;
                            }
                            new Handler().postDelayed(
                                    new Runnable() {
                                        public void run() {
                                            onSignupFailed();

                                            progressDialog.dismiss();
                                            _emailText.setText("");
                                            _passwordText.setText("");
                                            _reEnterPasswordText.setText("");
                                            _emailText.setSelection(0);
                                        }
                                    }, 3000);
                            return;

                        }
                    });

            appSingleton.addToRequestQueue(jsObjRequest, TAG);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the main
        goToLogin();
    }

    public void onSignupSuccess() {
        _signupButton.setEnabled(true);
        this.goToLogin();
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), string(R.string.e_email_found), Toast.LENGTH_LONG).show();
        _signupButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String name = _nameText.getText().toString();
        String lastname = _lastnameText.getText().toString();
        String email = _emailText.getText().toString();
        String district = _districtText.getSelectedItem().toString();
        String county = _countyText.getSelectedItem().toString();
        String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            _nameText.setError(string(R.string.e_valid_name));
            valid = false;
        } else {
            _nameText.setError(null);
        }

        if (lastname.isEmpty() || lastname.length() < 3) {
            _lastnameText.setError(string(R.string.e_valid_name));
            valid = false;
        } else {
            _lastnameText.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError(string(R.string.e_valid_email));
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (district.equals(string(R.string.e_district))) {
            Toast.makeText(this.getApplicationContext(), string(R.string.e_district), Toast.LENGTH_SHORT).show();
            valid = false;
        }

        if (county.equals(string(R.string.e_county))) {
            Toast.makeText(this.getApplicationContext(), string(R.string.e_county), Toast.LENGTH_SHORT).show();
            valid = false;
        }

        if (password.isEmpty()) {
            _passwordText.setError(string(R.string.e_empty_pwd));
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        if (reEnterPassword.isEmpty() || !(reEnterPassword.equals(password))) {
            _reEnterPasswordText.setError(string(R.string.e_match_pwd));
            valid = false;
        } else {
            _reEnterPasswordText.setError(null);
        }

        return valid;
    }

    private String string(int string) {
        return this.getResources().getString(string);
    }


    private void goToLogin() {
        Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

}
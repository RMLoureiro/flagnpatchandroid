package pt.unl.fct.di.apdc.flagnpatch;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pt.unl.fct.di.apdc.flagnpatch.Database.DBHandler;
import pt.unl.fct.di.apdc.flagnpatch.Resources.General.TokenData;
import pt.unl.fct.di.apdc.flagnpatch.Resources.Occurrence;
import pt.unl.fct.di.apdc.flagnpatch.Utils.AppSingleton;
import pt.unl.fct.di.apdc.flagnpatch.Utils.PermissionUtils;

public class MapsActivityWorker extends AppCompatActivity
        implements
        OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback {
    private static final String TAG = "MapsActivity";
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    /**
     * Request code for location permission request.
     *
     * @see #onRequestPermissionsResult(int, String[], int[])
     */
    static GoogleMap mMap;
    private List<Occurrence> reports;
    private AppSingleton appSingleton;
    private DBHandler datastore;
    private JSONObject tokenObj;

    private Boolean exit = false;

    /**
     * Flag indicating whether a requested permission has been denied after returning in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     */
    private boolean mPermissionDenied = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        appSingleton = AppSingleton.getInstance(this);
        tokenObj = new JSONObject();
        datastore = new DBHandler(this);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (datastore.getToken() != null) {
            TokenData token = datastore.getToken();
            try {
                tokenObj.accumulate(string(R.string.t_user), token.getUser());
                tokenObj.accumulate(string(R.string.t_id), token.getId());
                tokenObj.accumulate(string(R.string.t_creation), token.getCreationDate());
                tokenObj.accumulate(string(R.string.t_expiration), token.getExpirationDate());
                tokenObj.accumulate(string(R.string.t_role), token.getRole());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else tokenLost();


        MapFragment mapFragment = (MapFragment) this.getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mMap != null)
            mMap.clear();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (appSingleton.myReports.isEmpty() || !appSingleton.myReports.equals(reports)) {
            reports = new ArrayList<>();
            getAllReports(null);
        } else {
            reports = appSingleton.myReports;
            setAllMarkers(reports);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (appSingleton.myReports.isEmpty() || !appSingleton.myReports.equals(reports)) {
            reports = new ArrayList<>();
            getAllReports(null);
        } else
            reports = appSingleton.myReports;
            setAllMarkers(reports);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (appSingleton.myReports.isEmpty() || !appSingleton.myReports.equals(reports)) {
            mMap.clear();
            reports = new ArrayList<>();
            getAllReports(null);
        } else {
            reports = appSingleton.myReports;
            setAllMarkers(reports);
        }

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                LatLng latLng = new LatLng(point.latitude, point.longitude);
                mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
            }
        });


        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker.getTag() != null)
                    goToSpecificOccurence((Occurrence) marker.getTag());
                else
                    marker.showInfoWindow();
                return true;
            }
        });
        LatLng almada = new LatLng(38.6359837, -9.1874799);
        mMap.moveCamera(CameraUpdateFactory.zoomTo(12));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(almada));
        enableMyLocation();

        findViewById(R.id.addReport).setVisibility(View.GONE);
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (mMap != null) {
            // Access to the location has been granted to the app.
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }


    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, string(R.string.back_again),
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    public void setAllMarkers(List<Occurrence> all) {
        boolean pendente = appSingleton.pendente;
        boolean resolucao = appSingleton.resolucao;
        boolean resolvido = appSingleton.resolvido;
        boolean rejeitado = appSingleton.rejeitado;

        List<Occurrence> allMarkers = all;
        if (allMarkers.isEmpty()) {
            return;
        }
        for (Occurrence o : allMarkers) {
            if ((o.getState().equals(string(R.string.st_pendente)) && pendente) ||
                    (o.getState().equals(string(R.string.st_em_resolucao)) && resolucao) ||
                    (o.getState().equals(string(R.string.st_resolvido)) && resolvido) ||
                    (o.getState().equals(string(R.string.st_rejeitado)) && rejeitado)) {
                int color = -1;
                switch (o.getState()) {
                    case "Pendente":
                        color = R.mipmap.ic_marker_grey;
                        break;
                    case "Em Resolução":
                        color = R.mipmap.ic_marker_yellow;
                        break;
                    case "Resolvido":
                        color = R.mipmap.ic_marker_green;
                        break;
                    case "Rejeitado":
                        color = R.mipmap.ic_marker_red;
                        break;
                }
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(o.getLatitude(), o.getLongitude()))
                        .icon(BitmapDescriptorFactory.fromResource(color))).setTag(o);
            }
        }
    }

    private void goToSpecificOccurence(Occurrence occ) {
        Intent intent = new Intent(this, OccDetailsWorker.class);
        Bundle args = new Bundle();
        args.putParcelable(string(R.string.o_occ), occ);
        intent.putExtra(string(R.string.bundle), args);
        startActivity(intent);
    }

    public void getAllReports(String cursor) {
        JSONObject request = new JSONObject();
        try {
            if (cursor != null) {
                request.accumulate(string(R.string.j_cursor), cursor);
            }
            request.accumulate(string(R.string.j_token), tokenObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, string(R.string.ser_reports_worker), request, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray reportsRecieved = new JSONArray();
                try {
                    reportsRecieved = response.getJSONArray(string(R.string.j_report));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                List<Occurrence> occs = new ArrayList<>();
                if (reportsRecieved.length() > 0) {
                    for (int i = 0; i < reportsRecieved.length(); i++) {
                        try {
                            JSONObject obj = reportsRecieved.getJSONObject(i);
                            JSONObject addr = obj.getJSONObject(string(R.string.j_address));
                            Occurrence occ = new Occurrence(
                                    obj.getString(string(R.string.j_type)),
                                    obj.getString(string(R.string.j_description)),
                                    obj.getString(string(R.string.j_imageUrl)),
                                    obj.getString(string(R.string.j_addressAsStreet)),
                                    addr.getString(string(R.string.j_district)),
                                    addr.getString(string(R.string.j_county)),
                                    obj.getString(string(R.string.t_id)),
                                    obj.getInt(string(R.string.j_priority)),
                                    obj.getString(string(R.string.j_state)),
                                    obj.getDouble(string(R.string.j_latitude)),
                                    obj.getDouble(string(R.string.j_longitude)),
                                    obj.getString(string(R.string.j_creationDate)),
                                    obj.getString(string(R.string.j_followers)),
                                    obj.getBoolean(string(R.string.j_isFollowing)));
                            reports.add(occ);
                            appSingleton.myReports.add(occ);
                            occs.add(occ);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    try {
                        setAllMarkers(occs);
                        getAllReports(response.getString(string(R.string.j_cursor)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                if (error == null || error.networkResponse == null) {
                    return;
                } else {
                    if (reports.isEmpty()) {
                        return;
                    }
                    if (error.networkResponse.statusCode == 401) {
                        tokenLost();
                    }
                    if (reports == null) {
                        return;
                    }
                }
            }
        });

        appSingleton.addToRequestQueue(req, TAG);
    }


    private void tokenLost() {
        Toast.makeText(this, string(R.string.e_expired_session), Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
        datastore.deleteTable();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    private String string(int string) {
        return this.getResources().getString(string);
    }

}
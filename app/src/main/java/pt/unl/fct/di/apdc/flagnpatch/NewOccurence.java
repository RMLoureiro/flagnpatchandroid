package pt.unl.fct.di.apdc.flagnpatch;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.google.android.gms.maps.model.LatLng;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ViewListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import pt.unl.fct.di.apdc.flagnpatch.Database.DBHandler;
import pt.unl.fct.di.apdc.flagnpatch.Resources.General.TokenData;
import pt.unl.fct.di.apdc.flagnpatch.Resources.Occurrence;
import pt.unl.fct.di.apdc.flagnpatch.Utils.AppHelper;
import pt.unl.fct.di.apdc.flagnpatch.Utils.AppSingleton;
import pt.unl.fct.di.apdc.flagnpatch.Utils.CustomJsonArrayRequest;
import pub.devrel.easypermissions.EasyPermissions;

import static android.os.Build.VERSION_CODES.M;

public class NewOccurence extends AppCompatActivity {
    private static final String TAG = "NewOccurence";
    private static final int RESULT_LOAD_IMAGE = 1;
    private static final int REQUEST_IMAGE_CAPTURE = 2;
    private static final String NO_PHOTO = "/img/missing.png";
    private static final String DEFAULT_PHOTO = "https://itviec.com/assets/missing.png";
    @Bind(R.id.occ_photo)
    ImageButton _photoButton;
    @Bind(R.id.occ_type)
    Spinner _typeText;
    @Bind(R.id.occ_priority)
    SeekBar _priorityText;
    @Bind(R.id.occ_description)
    EditText _descriptionText;
    @Bind(R.id.occ_address)
    EditText _addressText;
    @Bind(R.id.btn_create)
    Button _createOccButton;
    CarouselView customCarouselView;
    private Uri mCapturedImageURI;
    private Geocoder geocoder;
    private DBHandler datastore;
    private AppSingleton appSingleton;
    private JSONObject tokenObj;
    private List<Address> temp;
    private Address addressGeo;
    private LatLng positionLatLng;
    private String addr;
    private List<String> types;
    private Bitmap bitmap;
    private String picturePath;
    private String filename;
    private List<Occurrence> suggestions;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        datastore = new DBHandler(this);
        appSingleton = AppSingleton.getInstance(this);

        picturePath = null;
        filename = null;

        Bundle bundle = getIntent().getParcelableExtra("bundle");
        positionLatLng = bundle.getParcelable("address");

        geocoder = new Geocoder(this);

        try {
            temp = geocoder.getFromLocation(positionLatLng.latitude, positionLatLng.longitude, 2);
            addressGeo = temp.get(1);

            if (addressGeo != null) {
                addr = "";
                for (int i = 0; i < addressGeo.getMaxAddressLineIndex(); i++) {
                    addr += addressGeo.getAddressLine(i) + " ";
                }
            } else
                addr = "" + addressGeo.getLatitude() + ", " + addressGeo.getLongitude();
        } catch (IOException e) {
            e.printStackTrace();
        }


        tokenObj = new JSONObject();
        JSONObject jsonToken = new JSONObject();

        if (datastore.getToken() != null) {
            TokenData token = datastore.getToken();
            try {
                tokenObj.accumulate(string(R.string.t_user), token.getUser());
                tokenObj.accumulate(string(R.string.t_id), token.getId());
                tokenObj.accumulate(string(R.string.t_creation), token.getCreationDate());
                tokenObj.accumulate(string(R.string.t_expiration), token.getExpirationDate());
                tokenObj.accumulate(string(R.string.t_role), token.getRole());
                jsonToken.accumulate(string(R.string.j_token), tokenObj);
                jsonToken.accumulate(string(R.string.j_county), addressGeo.getSubAdminArea());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else tokenLost();

        setContentView(R.layout.activity_new_occ);
        ButterKnife.bind(this);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        _photoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                btnAddOnClick(v);
            }
        });

        _createOccButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    createOcc();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        if (appSingleton.types.isEmpty() || !appSingleton.types.equals(types)) {

            types = new ArrayList<String>();
            types.add(string(R.string.o_type));

            JsonRequest jsArrRequest = new CustomJsonArrayRequest
                    (Request.Method.POST, string(R.string.ser_types), jsonToken, new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(final JSONArray response) {
                            if (response != null) {
                                try {
                                    for (int i = 0; i < response.length(); i++) {
                                        types.add(response.getJSONObject(i).get(string(R.string.j_name)).toString());
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                        android.R.layout.simple_spinner_dropdown_item, types) {
                                    @Override
                                    public boolean isEnabled(int position) {
                                        return position != 0;
                                    }

                                    @Override
                                    public View getDropDownView(int position, View convertView,
                                                                ViewGroup parent) {
                                        View view = super.getDropDownView(position, convertView, parent);
                                        TextView tv = (TextView) view;
                                        if (position == 0)
                                            tv.setTextColor(Color.GRAY);
                                        else
                                            tv.setTextColor(Color.WHITE);

                                        return view;
                                    }
                                };
                                _typeText.setAdapter(typeAdapter);
                            }

                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error == null || error.networkResponse == null) {
                                return;
                            }
                            if (error.networkResponse.statusCode == 401)
                                tokenLost();
                            else {
                                Toast.makeText(getBaseContext(), string(R.string.e_server_down), Toast.LENGTH_LONG).show();
                            }
                        }
                    });

            appSingleton.addToRequestQueue(jsArrRequest, TAG);
        } else types = appSingleton.types;

        _addressText.setEnabled(true);
        _addressText.setText(addr);
        _addressText.setEnabled(false);

        _typeText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                suggestions = new ArrayList<>();
                if (position > 0) {
                    JSONObject objJSON = new JSONObject();
                    try {
                        objJSON.accumulate(string(R.string.j_token), tokenObj);
                        objJSON.accumulate(string(R.string.j_type), types.get(position));
                        objJSON.accumulate(string(R.string.j_latitude), positionLatLng.latitude);
                        objJSON.accumulate(string(R.string.j_longitude), positionLatLng.longitude);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    JsonRequest type = new CustomJsonArrayRequest(
                            Request.Method.POST, string(R.string.ser_suggestions), objJSON, new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(final JSONArray response) {
                            if (response != null) {
                                try {
                                    for (int i = 0; i < response.length(); i++) {
                                        JSONObject obj = response.getJSONObject(i);
                                        JSONObject addr = obj.getJSONObject(string(R.string.j_address));
                                        Occurrence occ = new Occurrence(
                                                obj.getString(string(R.string.j_type)),
                                                obj.getString(string(R.string.j_description)),
                                                obj.getString(string(R.string.j_imageUrl)),
                                                obj.getString(string(R.string.j_addressAsStreet)),
                                                addr.getString(string(R.string.j_district)),
                                                addr.getString(string(R.string.j_county)),
                                                obj.getString(string(R.string.t_id)),
                                                obj.getInt(string(R.string.j_priority)),
                                                obj.getString(string(R.string.j_state)),
                                                obj.getDouble(string(R.string.j_latitude)),
                                                obj.getDouble(string(R.string.j_longitude)),
                                                obj.getString(string(R.string.j_creationDate)),
                                                obj.getString(string(R.string.j_followers)),
                                                obj.getBoolean(string(R.string.j_isFollowing)));
                                        suggestions.add(occ);
                                    }
                                    getSuggestion();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error == null || error.networkResponse == null) {
                                return;
                            } else {
                                if (suggestions.isEmpty())
                                    return;
                                if (error.networkResponse.statusCode == 401)
                                    tokenLost();
                                else {
                                    System.out.println(error.networkResponse);
                                    Toast.makeText(getBaseContext(), string(R.string.e_server_down), Toast.LENGTH_LONG).show();
                                }
                            }

                        }
                    });

                    appSingleton.addToRequestQueue(type, TAG);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void createOcc() throws IOException {
        Log.d(TAG, "CreateOcc");

        if (!validate()) {
            onCreateFailed();
            return;
        }

        _createOccButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(NewOccurence.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(string(R.string.o_creating));
        progressDialog.show();

        addressGeo = temp.get(1);

        String type = _typeText.getSelectedItem().toString();
        int priority = _priorityText.getProgress() + 1;
        String description = _descriptionText.getText().toString();
        String imageUrl = hasPicturePath();
        String addressAsStreet = addr;
        double latitude = positionLatLng.latitude;
        double longitude = positionLatLng.longitude;
        String district = addressGeo.getAdminArea();
        String county = addressGeo.getSubAdminArea();

        if (imageUrl == null) {
            new Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            onCreateFailed();
                            progressDialog.dismiss();
                            _typeText.setSelection(0);
                            _priorityText.setProgress(2);
                        }
                    }, 3000);
            return;
        }

        try {
            JSONObject credentials = new JSONObject();
            credentials.accumulate(string(R.string.j_type), type);
            credentials.accumulate(string(R.string.j_priority), priority);
            credentials.accumulate(string(R.string.j_description), description);
            credentials.accumulate(string(R.string.j_imageUrl), imageUrl);
            credentials.accumulate(string(R.string.j_addressAsStreet), addressAsStreet);
            credentials.accumulate(string(R.string.j_latitude), latitude);
            credentials.accumulate(string(R.string.j_longitude), longitude);

            JSONObject address = new JSONObject();
            address.accumulate(string(R.string.j_district), district);
            address.accumulate(string(R.string.j_county), county);

            credentials.accumulate(string(R.string.j_address), address);

            JSONObject report = new JSONObject();
            report.accumulate(string(R.string.j_token), tokenObj);
            report.accumulate(string(R.string.j_get_report), credentials);

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.POST, string(R.string.ser_regist_report), report, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Occurrence occ = new Occurrence(
                                        response.getString(string(R.string.j_type)),
                                        response.getString(string(R.string.j_description)),
                                        response.getString(string(R.string.j_imageUrl)),
                                        response.getString(string(R.string.j_addressAsStreet)),
                                        response.getJSONObject(string(R.string.j_address)).getString(string(R.string.j_district)),
                                        response.getJSONObject(string(R.string.j_address)).getString(string(R.string.j_county)),
                                        response.getString(string(R.string.t_id)),
                                        response.getInt(string(R.string.j_priority)),
                                        response.getString(string(R.string.j_state)),
                                        response.getDouble(string(R.string.j_latitude)),
                                        response.getDouble(string(R.string.j_longitude)),
                                        response.getString(string(R.string.j_creationDate)),
                                        response.getString(string(R.string.j_followers)),
                                        response.getBoolean(string(R.string.j_isFollowing)));
                                appSingleton.myReports.add(occ);
                                appSingleton.allReports.add(occ);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            new Handler().postDelayed(
                                    new Runnable() {
                                        public void run() {
                                            onCreateSuccess();
                                            progressDialog.dismiss();
                                        }
                                    }, 3000);
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error == null || error.networkResponse == null) {
                                return;
                            }
                            if (error.networkResponse.statusCode == 401)
                                tokenLost();
                            new Handler().postDelayed(
                                    new Runnable() {
                                        public void run() {
                                            onCreateFailed();
                                            progressDialog.dismiss();
                                            _typeText.setSelection(0);
                                            _priorityText.setProgress(2);
                                        }
                                    }, 3000);
                            return;

                        }
                    });

            appSingleton.addToRequestQueue(jsObjRequest, TAG);


        } catch (JSONException f) {
            f.printStackTrace();
        }

    }

    private String hasPicturePath() {
        if (picturePath == null)
            return NO_PHOTO;
        else {
            final boolean[] loaded = {false};
            final ProgressDialog loading = ProgressDialog.show(this, string(R.string.o_loading),
                    string(R.string.o_pleaseWait), false, false);

            try {
                JSONObject json = new JSONObject();
                json.accumulate(string(R.string.j_image), getStringImage(bitmap));
                URL url = new URL(string(R.string.ser_photo_db) + filename);
                int response = AppHelper.doPOST(url, json);
                if (response == 200) {
                    loading.dismiss();
                    loaded[0] = true;
                } else {
                    System.out.println(response);
                    loading.dismiss();
                    if (response == 401)
                        tokenLost();

                    Toast.makeText(getApplicationContext(), string(R.string.e_photo_down), Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (loaded[0])
                return string(R.string.ser_photo_db) + filename;
            return null;
        }

    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    @Override
    public void onBackPressed() {
        goToMaps();
    }

    public void onCreateSuccess() {
        _createOccButton.setEnabled(true);
        finish();
        goToMaps();
    }

    public void onCreateFailed() {
        Toast.makeText(getBaseContext(), string(R.string.e_invalid_occ_parameters), Toast.LENGTH_LONG).show();

        _createOccButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String type = _typeText.getSelectedItem().toString();
        String description = _descriptionText.getText().toString();

        if (type.equals(string(R.string.o_type))) {
            Toast.makeText(this.getApplicationContext(), string(R.string.e_invalid_type), Toast.LENGTH_SHORT).show();
            valid = false;
        }

        if (description.isEmpty()) {
            _descriptionText.setError(string(R.string.e_empty_description));
            valid = false;
        } else {
            _descriptionText.setError(null);
        }

        return valid;
    }

    private void goToMaps() {
        Intent intent = new Intent(NewOccurence.this, MainActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    private String string(int string) {
        return this.getResources().getString(string);
    }

    public void btnAddOnClick(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_dialog_box);
        dialog.setTitle(string(R.string.o_choose_photo));
        Button btnExit = (Button) dialog.findViewById(R.id.exitDialog);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.findViewById(R.id.btnChoosePath).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeGallery();
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.btnTakePhoto).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = M)
            @Override
            public void onClick(View v) {
                activeTakePhoto();
                dialog.dismiss();
            }
        });

    }

    /**
     * take a photo
     */
    private void activeTakePhoto() {
        if (Build.VERSION.SDK_INT >= M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        REQUEST_IMAGE_CAPTURE);
            } else
                doneCamera();
        } else
            Toast.makeText(this, string(R.string.e_version_below), Toast.LENGTH_LONG).show();

    }

    /**
     * to gallery
     */
    private void activeGallery() {
        String[] galleryPermissions = new String[2];
        galleryPermissions[0] = Manifest.permission.READ_EXTERNAL_STORAGE;
        galleryPermissions[1] = Manifest.permission.WRITE_EXTERNAL_STORAGE;

        if (EasyPermissions.hasPermissions(this, galleryPermissions)) {
            doneGallery();
        } else {
            EasyPermissions.requestPermissions(this, "Acesso à memória",
                    101, galleryPermissions);
        }
    }


    private void doneGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                doneCamera();
            } else {
                Toast.makeText(this, string(R.string.e_imp_camera), Toast.LENGTH_LONG).show();
            }
        }

    }

    private void doneCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                try {
                    Bundle extras = data.getExtras();

                    bitmap = (Bitmap) extras.get(string(R.string.o_data));

                    _photoButton.setImageBitmap(bitmap);

                    picturePath = android.os.Environment
                            .getExternalStorageDirectory().getAbsolutePath()
                            + File.separator
                            + string(R.string.a_name);

                    File dir = new File(picturePath);
                    if (!dir.exists())
                        dir.mkdirs();

                    filename = string(R.string.o_default) + "_" +
                            String.valueOf(System.currentTimeMillis()) + string(R.string.o_file);
                    File file = new File(dir, filename);


                    try {
                        FileOutputStream outFile = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                        outFile.flush();
                        outFile.close();
                        makeSureFileWasCreated(file);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                this.alreadyChoosed();
            } else if (requestCode == RESULT_LOAD_IMAGE) {

                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                picturePath = c.getString(columnIndex);
                _photoButton.setImageURI(selectedImage);
                c.close();
                bitmap = (BitmapFactory.decodeFile(picturePath));
                this.alreadyChoosed();
            }
        }
    }

    private void makeSureFileWasCreated(File file) {
        MediaScannerConnection.scanFile(this,
                new String[]{file.toString()},
                null,
                new MediaScannerConnection.OnScanCompletedListener() {

                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                        Log.e("ExternalStorage", "scanned" + path + ":");
                        Log.e("ExternalStorage", "-> uri=" + uri);

                    }
                });
    }

    private void getSuggestion() {
        final List<Occurrence> sugg = suggestions;
        if (!suggestions.isEmpty()) {
            customCarouselView = (CarouselView) findViewById(R.id.carouselView);
            customCarouselView.setSlideInterval(10000);
            ViewListener viewListener = new ViewListener() {
                @Override
                public View setViewForPosition(int position) {

                    View customView = getLayoutInflater().inflate(R.layout.item_sugg, null);

                    final ImageView imageSugg = (ImageView) customView.findViewById(R.id.occImageView);
                    TextView addressSugg = (TextView) customView.findViewById(R.id.labelAddress);
                    TextView descriptionSugg = (TextView) customView.findViewById(R.id.labelDescription);
                    final Button follow = (Button) customView.findViewById(R.id.btn_follow);

                    final Occurrence pos = sugg.get(position);

                    if (!pos.getFollowed()) {
                        follow.setBackgroundColor(Color.parseColor("#0099FF"));
                        follow.setText(string(R.string.o_follow));
                    } else {
                        follow.setBackgroundColor(Color.parseColor("#FF1A1A"));
                        follow.setText(string(R.string.o_unfollow));
                    }


                    follow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!pos.getFollowed()) {
                                AlertDialog.Builder b = new AlertDialog.Builder(new ContextThemeWrapper(NewOccurence.this, R.style.AppTheme_Dark));
                                b.setTitle(string(R.string.o_sugestion_priority));
                                String[] types = {"1", "2", "3", "4", "5"};
                                b.setItems(types, new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        int newPriority = which + 1;

                                        JSONObject request = new JSONObject();
                                        try {
                                            request.accumulate(string(R.string.j_token), tokenObj);
                                            request.accumulate(string(R.string.j_priority), newPriority);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, string(R.string.ser_follow) + pos.getId()
                                                , request, new Response.Listener<JSONObject>() {

                                            @Override
                                            public void onResponse(JSONObject response) {
                                                follow.setBackgroundColor(Color.parseColor("#FF1A1A"));
                                                follow.setText(string(R.string.o_unfollow));
                                                pos.followed = true;
                                                appSingleton.myReports.add(pos);
                                                goToMaps();
                                            }
                                        }, new Response.ErrorListener() {

                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                if (error == null || error.networkResponse == null) {
                                                    return;
                                                }
                                            }
                                        });
                                        appSingleton.addToRequestQueue(jsObjRequest, TAG);

                                    }

                                });
                                b.show();


                            } else {

                                JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, string(R.string.ser_unfollow) + pos.getId()
                                        , tokenObj, new Response.Listener<JSONObject>() {

                                    @Override
                                    public void onResponse(JSONObject response) {
                                        follow.setBackgroundColor(Color.parseColor("#0099FF"));
                                        follow.setText(string(R.string.o_follow));
                                        pos.followed = false;
                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        if (error == null || error.networkResponse == null) {
                                            return;
                                        }
                                    }
                                });
                                appSingleton.addToRequestQueue(jsObjRequest, TAG);
                            }
                        }
                    });

                    if (pos.getImageURL().equals(NO_PHOTO) ||
                            pos.getImageURL().equals(DEFAULT_PHOTO)) {

                    } else {
                        ImageRequest imageRequest = new ImageRequest(
                                pos.getImageURL(), // Image URL
                                new Response.Listener<Bitmap>() {
                                    @Override
                                    public void onResponse(Bitmap response) {
                                        Bitmap thumbImage = ThumbnailUtils.extractThumbnail(
                                                response, 64, 64);
                                        imageSugg.setImageBitmap(thumbImage);
                                    }
                                },
                                115, // Image width
                                115, // Image height
                                ImageView.ScaleType.CENTER, // Image scale type
                                Bitmap.Config.RGB_565, //Image decode configuration
                                new Response.ErrorListener() { // Error listener
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        // Do something with error response
                                        if (error == null || error.networkResponse == null) {
                                            return;
                                        }
                                    }
                                }
                        );
                        appSingleton.addToRequestQueue(imageRequest, TAG);
                    }
                    addressSugg.setText(pos.getAddress());
                    descriptionSugg.setText(pos.getDescription());

                    customCarouselView.setIndicatorGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);

                    return customView;
                }
            };

            customCarouselView.setViewListener(viewListener);
            findViewById(R.id.containerSuggestion).setVisibility(View.VISIBLE);
            customCarouselView.setPageCount(sugg.size());


        }
    }

    private void alreadyChoosed() {
        int color = getResources().getColor(R.color.primary);
        findViewById(R.id.stroke).setBackgroundColor(color);
        _photoButton.setBackgroundColor(color);
        _photoButton.setScaleType(ImageView.ScaleType.FIT_CENTER);
        _photoButton.setEnabled(false);
    }

    private void tokenLost() {
        Toast.makeText(this, string(R.string.e_expired_session), Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
        datastore.deleteTable();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }


}
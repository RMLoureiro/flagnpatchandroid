package pt.unl.fct.di.apdc.flagnpatch;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pt.unl.fct.di.apdc.flagnpatch.Database.DBHandler;
import pt.unl.fct.di.apdc.flagnpatch.Resources.General.TokenData;
import pt.unl.fct.di.apdc.flagnpatch.Resources.Occurrence;
import pt.unl.fct.di.apdc.flagnpatch.Utils.AppSingleton;
import pt.unl.fct.di.apdc.flagnpatch.Utils.ListAdapter;
import pt.unl.fct.di.apdc.flagnpatch.Utils.RecyclerTouchListener;


public class ListWorkerActivity extends AppCompatActivity {

    private static final String TAG = "ListActivity";


    private List<Occurrence> allReports;
    private RecyclerView recyclerView;
    private ListAdapter mAdapter;

    private AppSingleton appSingleton;
    private DBHandler datastore;
    private JSONObject tokenObj;
    private boolean exit = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycle_view_worker);

        appSingleton = AppSingleton.getInstance(this);
        datastore = new DBHandler(this);
        tokenObj = new JSONObject();
        allReports = new ArrayList<>();

        if (datastore.getToken() != null) {
            TokenData token = datastore.getToken();
            try {
                tokenObj.accumulate(string(R.string.t_user), token.getUser());
                tokenObj.accumulate(string(R.string.t_id), token.getId());
                tokenObj.accumulate(string(R.string.t_creation), token.getCreationDate());
                tokenObj.accumulate(string(R.string.t_expiration), token.getExpirationDate());
                tokenObj.accumulate(string(R.string.t_role), token.getRole());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else tokenLost();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerListWorker);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Occurrence occ = allReports.get(position);
                Intent intent = new Intent(getApplicationContext(), OccDetailsWorker.class);
                Bundle args = new Bundle();
                args.putParcelable(string(R.string.o_occ), occ);
                intent.putExtra(string(R.string.bundle), args);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ListWorkerActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        if (appSingleton.allReports.isEmpty() || !appSingleton.allReports.equals(allReports)) {
            allReports = new ArrayList<>();
            getAllReports(null);
        } else {
            allReports = appSingleton.allReports;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ListWorkerActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    public void getAllReports(String cursor) {
        JSONObject request = new JSONObject();
        try {
            if (cursor != null) {
                request.accumulate(string(R.string.j_cursor), cursor);
            }
            request.accumulate(string(R.string.j_token), tokenObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, string(R.string.ser_reports_worker), request, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray reportsRecieved = new JSONArray();
                try {
                    reportsRecieved = response.getJSONArray(string(R.string.j_report));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                boolean pendente = appSingleton.pendente;
                boolean resolucao = appSingleton.resolucao;
                boolean resolvido = appSingleton.resolvido;
                boolean rejeitado = appSingleton.rejeitado;
                if (reportsRecieved.length() > 0) {
                    for (int i = 0; i < reportsRecieved.length(); i++) {
                        try {
                            JSONObject obj = reportsRecieved.getJSONObject(i);
                            JSONObject addr = obj.getJSONObject(string(R.string.j_address));
                            Occurrence occ = new Occurrence(
                                    obj.getString(string(R.string.j_type)),
                                    obj.getString(string(R.string.j_description)),
                                    obj.getString(string(R.string.j_imageUrl)),
                                    obj.getString(string(R.string.j_addressAsStreet)),
                                    addr.getString(string(R.string.j_district)),
                                    addr.getString(string(R.string.j_county)),
                                    obj.getString(string(R.string.t_id)),
                                    obj.getInt(string(R.string.j_priority)),
                                    obj.getString(string(R.string.j_state)),
                                    obj.getDouble(string(R.string.j_latitude)),
                                    obj.getDouble(string(R.string.j_longitude)),
                                    obj.getString(string(R.string.j_creationDate)),
                                    obj.getString(string(R.string.j_followers)),
                                    obj.getBoolean(string(R.string.j_isFollowing)));
                            if ((occ.getState().equals(string(R.string.st_pendente)) && pendente) ||
                                    (occ.getState().equals(string(R.string.st_em_resolucao)) && resolucao) ||
                                    (occ.getState().equals(string(R.string.st_resolvido)) && resolvido) ||
                                    (occ.getState().equals(string(R.string.st_rejeitado)) && rejeitado)) {
                                allReports.add(occ);
                                appSingleton.allReports.add(occ);
                            } else continue;

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    try {
                        getAllReports(response.getString(string(R.string.j_cursor)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error == null || error.networkResponse == null) {
                    mAdapter = new ListAdapter(allReports);
                    recyclerView.setAdapter(mAdapter);
                } else {
                    if (allReports.isEmpty()) {
                        return;
                    }
                    if (error.networkResponse.statusCode == 401) {
                        tokenLost();
                    }
                    if (allReports == null) {
                        return;
                    }
                }
            }
        });

        appSingleton.addToRequestQueue(req, TAG);
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, string(R.string.back_again),
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }


    private void tokenLost() {
        Toast.makeText(this, string(R.string.e_expired_session), Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
        datastore.deleteTable();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    private String string(int string) {
        return getResources().getString(string);
    }

}
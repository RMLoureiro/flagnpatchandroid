package pt.unl.fct.di.apdc.flagnpatch;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pt.unl.fct.di.apdc.flagnpatch.Database.DBHandler;
import pt.unl.fct.di.apdc.flagnpatch.Resources.General.TokenData;
import pt.unl.fct.di.apdc.flagnpatch.Resources.Occurrence;
import pt.unl.fct.di.apdc.flagnpatch.Utils.AppSingleton;
import pt.unl.fct.di.apdc.flagnpatch.Utils.CustomJsonArrayRequest;

public class FiltersActivity extends AppCompatActivity {

    CheckedTextView ctv_pendente, ctv_resolucao, ctv_resolvido, ctv_rejeitado;

    private AppSingleton appSingleton;
    private boolean pendente = true;
    private boolean resolucao = true;
    private boolean resolvido = true;
    private boolean rejeitado = true;
    private JSONObject tokenObj;
    private DBHandler datastore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters);
        datastore = new DBHandler(this);

        appSingleton = AppSingleton.getInstance(getApplicationContext());

        tokenObj = new JSONObject();
        TokenData token;
        if (datastore.getToken() != null) {
            token = datastore.getToken();
            try {
                tokenObj.accumulate(string(R.string.t_user), token.getUser());
                tokenObj.accumulate(string(R.string.t_id), token.getId());
                tokenObj.accumulate(string(R.string.t_creation), token.getCreationDate());
                tokenObj.accumulate(string(R.string.t_expiration), token.getExpirationDate());
                tokenObj.accumulate(string(R.string.t_role), token.getRole());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else tokenLost();

        //getNotifications();
        try {
            if(!tokenObj.getString(string(R.string.t_role)).equals(string(R.string.w_work)))
                getNotifications();
            else
                findViewById(R.id.notification_name).setVisibility(View.GONE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ctv_pendente = (CheckedTextView) findViewById(R.id.ctv_pendente);
        ctv_resolucao = (CheckedTextView) findViewById(R.id.ctv_resolucao);
        ctv_resolvido = (CheckedTextView) findViewById(R.id.ctv_resolvido);
        ctv_rejeitado = (CheckedTextView) findViewById(R.id.ctv_rejeitado);

        ctv_pendente.setChecked(pendente);
        ctv_resolucao.setChecked(resolucao);
        ctv_resolvido.setChecked(resolvido);
        ctv_rejeitado.setChecked(rejeitado);

        ctv_pendente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pendente = !pendente;
                ctv_pendente.setChecked(pendente);
                appSingleton.pendente = pendente;
                appSingleton.allReports = new ArrayList<>();
                appSingleton.myReports = new ArrayList<>();
            }
        });
        ctv_resolucao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resolucao = !resolucao;
                ctv_resolucao.setChecked(resolucao);
                appSingleton.resolucao = resolucao;
                appSingleton.allReports = new ArrayList<>();
                appSingleton.myReports = new ArrayList<>();
            }
        });
        ctv_resolvido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resolvido = !resolvido;
                ctv_resolvido.setChecked(resolvido);
                appSingleton.resolvido = resolvido;
                appSingleton.allReports = new ArrayList<>();
                appSingleton.myReports = new ArrayList<>();
            }
        });
        ctv_rejeitado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rejeitado = !rejeitado;
                ctv_rejeitado.setChecked(rejeitado);
                appSingleton.rejeitado = rejeitado;
                appSingleton.allReports = new ArrayList<>();
                appSingleton.myReports = new ArrayList<>();
            }
        });
    }


    protected void getNotifications() {

        JsonRequest req = new CustomJsonArrayRequest(Request.Method.POST, string(R.string.ser_load_notification), tokenObj, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                LinearLayout layout = (LinearLayout) findViewById(R.id.notification_layout);
                LayoutInflater inflater = getLayoutInflater();
                View view = findViewById(R.id.notification_layout);
                JSONObject current;
                String previous = "";
                String next = "";

                boolean seen = true;
                if(response.length() > 0) {
                    for (int i = response.length() - 1; i >= 0; i--) {
                        String occId = "";
                        String rId = "";
                        try {
                            current = response.getJSONObject(i);
                            previous = current.getString(string(R.string.n_prevStatus));
                            next = current.getString(string(R.string.n_newStatus));
                            seen = current.getBoolean(string(R.string.n_wasSeen));
                            occId = current.getString(string(R.string.t_id));
                            rId = current.getString(string(R.string.n_reportId));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        View notificationView = inflater.inflate(R.layout.notification_element, (ViewGroup) view.getParent(), false);

                        final TextView content = (TextView) notificationView.findViewById(R.id.notification_content);
                        content.setText(string(R.string.n_notification1) + previous + string(R.string.n_notification2) + next);

                        if (!seen) {
                            content.setBackgroundColor(Color.parseColor(string(R.color.amarelo)));
                        }

                        final String id = occId;
                        final String reportId = rId;
                        notificationView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                JsonObjectRequest reqOcc = new JsonObjectRequest
                                        (Request.Method.POST, string(R.string.ser_report_byId) + reportId, tokenObj, new Response.Listener<JSONObject>() {

                                            @Override
                                            public void onResponse(JSONObject response) {
                                                JSONObject obj = response;
                                                JSONObject addr = null;
                                                Occurrence occ = null;
                                                try {
                                                    addr = obj.getJSONObject(string(R.string.j_address));
                                                    occ = new Occurrence(
                                                            obj.getString(string(R.string.j_type)),
                                                            obj.getString(string(R.string.j_description)),
                                                            obj.getString(string(R.string.j_imageUrl)),
                                                            obj.getString(string(R.string.j_addressAsStreet)),
                                                            addr.getString(string(R.string.j_district)),
                                                            addr.getString(string(R.string.j_county)),
                                                            obj.getString(string(R.string.t_id)),
                                                            obj.getInt(string(R.string.j_priority)),
                                                            obj.getString(string(R.string.j_state)),
                                                            obj.getDouble(string(R.string.j_latitude)),
                                                            obj.getDouble(string(R.string.j_longitude)),
                                                            null,
                                                            obj.getString(string(R.string.j_followers)),
                                                            obj.getBoolean(string(R.string.j_isFollowing)));
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                System.out.println(occ.followed + " " + occ.getFollowed());
                                                Intent intent = new Intent(FiltersActivity.this, OccDetailsUser.class);
                                                Bundle args = new Bundle();
                                                args.putParcelable(string(R.string.o_occ), occ);
                                                intent.putExtra(string(R.string.bundle), args);
                                                startActivity(intent);
                                            }
                                        }, new Response.ErrorListener() {

                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                if (error == null || error.networkResponse == null) {
                                                    return;
                                                }

                                            }
                                        });

                                appSingleton.addToRequestQueue(reqOcc, string(R.string.n_normalOcc));


                                JsonObjectRequest seen = new JsonObjectRequest
                                        (Request.Method.PUT, string(R.string.ser_see_notification) + id, tokenObj, new Response.Listener<JSONObject>() {

                                            @Override
                                            public void onResponse(JSONObject response) {
                                                content.setBackgroundColor(Color.parseColor(string(R.color.iron)));
                                            }
                                        }, new Response.ErrorListener() {

                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                if (error == null || error.networkResponse == null) {
                                                    return;
                                                }

                                            }
                                        });

                                appSingleton.addToRequestQueue(seen, string(R.string.n_checkSeen));
                            }

                        });


                        layout.addView(notificationView);


                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error == null || error.networkResponse == null) {
                    LinearLayout layout = (LinearLayout) findViewById(R.id.notification_layout);
                    LayoutInflater inflater = getLayoutInflater();
                    View view = findViewById(R.id.notification_layout);

                    View notificationView1 = inflater.inflate(R.layout.notification_element, (ViewGroup) view.getParent(), false);

                    TextView content1 = (TextView) notificationView1.findViewById(R.id.notification_content);
                    content1.setText("Não tem notificações");
                    layout.addView(notificationView1);
                    return;
                }
                if (error.networkResponse.statusCode == 401) {
                    tokenLost();
                }
            }
        });

        appSingleton.addToRequestQueue(req, string(R.string.n_notif));
    }

    private void tokenLost() {
        Toast.makeText(this, string(R.string.e_expired_session), Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
        datastore.deleteTable();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    private String string(int string) {
        return this.getResources().getString(string);
    }
}
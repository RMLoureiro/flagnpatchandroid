package pt.unl.fct.di.apdc.flagnpatch;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import pt.unl.fct.di.apdc.flagnpatch.Database.DBHandler;
import pt.unl.fct.di.apdc.flagnpatch.Resources.General.TokenData;
import pt.unl.fct.di.apdc.flagnpatch.Resources.OccComments;
import pt.unl.fct.di.apdc.flagnpatch.Utils.AppSingleton;
import pt.unl.fct.di.apdc.flagnpatch.Utils.CustomJsonArrayRequest;


/**
 * A simple {@link Fragment} subclass.
 */
public class CommentsFragment extends Fragment {
    private static final String TAG = "Comments";
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

        }
    };
    private DBHandler datastore;
    private AppSingleton appSingleton;
    private List<OccComments> comments;

    public CommentsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        appSingleton = AppSingleton.getInstance(getContext());
        datastore = new DBHandler(getContext());
        final View view = inflater.inflate(R.layout.fragment_comments, container, false);
        String id = null;
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            id = bundle.getString(string(R.string.t_id));
        }

        final ProgressDialog progressDialog = new ProgressDialog(getContext(),
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(string(R.string.c_search_comm));
        progressDialog.show();

        TokenData token = datastore.getToken();
        JSONObject credentials = new JSONObject();
        try {
            credentials.accumulate(string(R.string.t_user), token.getUser());
            credentials.accumulate(string(R.string.t_id), token.getId());
            credentials.accumulate(string(R.string.t_creation), token.getCreationDate());
            credentials.accumulate(string(R.string.t_expiration), token.getExpirationDate());
            credentials.accumulate(string(R.string.t_role), token.getRole());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonRequest req = new CustomJsonArrayRequest(Request.Method.POST, string(R.string.ser_get_comments) + id, credentials, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                comments = new ArrayList<>();

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        comments.add(new OccComments(
                                obj.getString(string(R.string.c_content)),
                                obj.getString(string(R.string.c_author_email)),
                                obj.getString(string(R.string.c_author_name)),
                                obj.getInt(string(R.string.c_author_identifier)),
                                obj.getLong(string(R.string.c_register_time)),
                                obj.getString(string(R.string.c_register_date))));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                createComments(view, comments, inflater, container, progressDialog);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error == null || error.networkResponse == null) {
                    progressDialog.dismiss();
                    return;
                }
                progressDialog.dismiss();

            }
        });


        appSingleton.addToRequestQueue(req, TAG);

        return view;
    }

    private void createComments(View view, List<OccComments> comments, LayoutInflater inflater, ViewGroup container, ProgressDialog progressDialog) {
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.comment_list);

        Iterator<OccComments> it = comments.iterator();

        while (it.hasNext()) {
            View commentView = inflater.inflate(R.layout.comment_item_list, container, false);

            OccComments comment = it.next();
            System.out.println(comment);

            TextView description = (TextView) commentView.findViewById(R.id.description);
            TextView name = (TextView) commentView.findViewById(R.id.name);
            TextView authorId = (TextView) commentView.findViewById(R.id.author_id_detail);
            TextView date = (TextView) commentView.findViewById(R.id.date_detail);

            authorId.setText("(" + comment.authorId + ")");
            description.setText(comment.content);
            name.setText(comment.authorName);
            date.setText(comment.date);


            layout.addView(commentView);

        }
        progressDialog.dismiss();
    }

    private String string(int string) {
        return this.getResources().getString(string);
    }
}


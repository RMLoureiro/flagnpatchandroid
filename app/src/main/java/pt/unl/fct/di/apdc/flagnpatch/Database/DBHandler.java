package pt.unl.fct.di.apdc.flagnpatch.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import pt.unl.fct.di.apdc.flagnpatch.Resources.General.TokenData;

public class DBHandler extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "infotoken";
    // Contacts table name
    private static final String TABLE_TOKEN = "tokens";
    // Shops Table Columns names
    private static final String KEY_USER = "user";
    private static final String KEY_ID = "id";
    private static final String KEY_CREATION = "creation";
    private static final String KEY_EXPIRATION = "expiration";
    private static final String KEY_ROLE = "role";


    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TOKEN_TABLE = "CREATE TABLE " + TABLE_TOKEN + "("
                + KEY_USER + " TEXT PRIMARY KEY, "
                + KEY_ID + " TEXT NOT NULL, "
                + KEY_CREATION + " TEXT NOT NULL, "
                + KEY_EXPIRATION + " TEXT NOT NULL, "
                + KEY_ROLE + " TEXT NOT NULL);";
        db.execSQL(CREATE_TOKEN_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
// Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TOKEN);
// Creating tables again
        onCreate(db);
    }

    public void addToken(TokenData tokenData) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_TOKEN);
        ContentValues values = new ContentValues();
        values.put(KEY_USER, tokenData.getUser());
        values.put(KEY_ID, tokenData.getId());
        values.put(KEY_CREATION, tokenData.getCreationDate());
        values.put(KEY_EXPIRATION, tokenData.getExpirationDate());
        values.put(KEY_ROLE, tokenData.getRole());
// Inserting Row
        db.insert(TABLE_TOKEN, null, values);
        db.close();

    }

    public TokenData getToken() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_TOKEN, new String[]{" * "},
                null, null, null, null, null, null);
        TokenData token = null;
        if (cursor != null && cursor.moveToFirst()) {
            token = new TokenData(cursor.getString(0),
                    cursor.getString(1), cursor.getLong(2),
                    cursor.getLong(3), cursor.getString(4));
        }
        db.close();
// return shop
        return token;
    }

    public void deleteTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_TOKEN);
        db.close();
    }

}


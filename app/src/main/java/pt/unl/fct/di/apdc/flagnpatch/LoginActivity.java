package pt.unl.fct.di.apdc.flagnpatch;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import pt.unl.fct.di.apdc.flagnpatch.Database.DBHandler;
import pt.unl.fct.di.apdc.flagnpatch.Resources.General.TokenData;
import pt.unl.fct.di.apdc.flagnpatch.Utils.AppSingleton;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    private static final int REQUEST_MAPS = 0;
    @Bind(R.id.input_email)
    EditText _emailText;
    @Bind(R.id.input_password)
    EditText _passwordText;
    @Bind(R.id.btn_login)
    Button _loginButton;
    @Bind(R.id.link_signup)
    TextView _signupLink;
    @Bind(R.id.forgot_password)
    TextView _forgotPassword;

    private DBHandler datastore;
    private AppSingleton appSingleton;
    private boolean exit = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        datastore = new DBHandler(this);
        appSingleton = AppSingleton.getInstance(this);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    login();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        _signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
        _forgotPassword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(LoginActivity.this, R.style.AppTheme_Dark));
                builder.setTitle(string(R.string.fp_insert_pwd));

                // Set up the input
                final EditText input = new EditText(getApplicationContext());
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setSingleLine(false);
                input.setPadding(50, 50, 50, 50);
                input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        JSONObject request = new JSONObject();
                        try {
                            request.accumulate(string(R.string.fp_email), input.getText());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                                (Request.Method.POST, string(R.string.ser_reset_pwd), request, new Response.Listener<JSONObject>() {

                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Toast.makeText(getBaseContext(), string(R.string.fp_success), Toast.LENGTH_LONG).show();
                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getBaseContext(), string(R.string.fp_unsuccess), Toast.LENGTH_LONG).show();
                                    }
                                });

                        appSingleton.addToRequestQueue(jsObjRequest, TAG);
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

            }
        });
    }


    public void login() throws IOException {
        Log.d(TAG, "Login");
        if (!validate()) {
            onLoginFailed();
            return;
        }

        _loginButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(string(R.string.l_authenticating));
        progressDialog.show();

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        try {
            JSONObject credentials = new JSONObject();
            credentials.accumulate(string(R.string.j_email), email);
            credentials.accumulate(string(R.string.j_pwd), password);

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.POST, string(R.string.ser_login), credentials, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            TokenData token = null;
                            try {
                                token = new TokenData(
                                        response.getString(string(R.string.t_user)),
                                        response.getString(string(R.string.t_id)),
                                        response.getLong(string(R.string.t_creation)),
                                        response.getLong(string(R.string.t_expiration)),
                                        response.getString(string(R.string.t_role)));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (token.getRole().equals(string(R.string.l_end)) || token.getRole().equals(string(R.string.l_core))) {
                                Toast.makeText(LoginActivity.this, string(R.string.e_wrong_role), Toast.LENGTH_LONG).show();
                                return;
                            } else {
                                datastore.addToken(token);
                                new android.os.Handler().postDelayed(
                                        new Runnable() {
                                            public void run() {
                                                onLoginSuccess();
                                                // onLoginFailed();
                                                progressDialog.dismiss();
                                            }
                                        }, 3000);
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            new android.os.Handler().postDelayed(
                                    new Runnable() {
                                        public void run() {
                                            onLoginFailed();
                                            // onLoginFailed();
                                            progressDialog.dismiss();
                                            _passwordText.setText("");
                                        }
                                    }, 3000);
                            return;
                        }
                    });

            appSingleton.addToRequestQueue(jsObjRequest, TAG);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onLoginSuccess() {
        _loginButton.setEnabled(true);
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivityForResult(intent, REQUEST_MAPS);
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), string(R.string.e_invalid_email_pwd), Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError(string(R.string.e_valid_email));
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty()) {
            _passwordText.setError(string(R.string.e_empty_pwd));
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, string(R.string.back_again),
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }

    private String string(int string) {
        return this.getResources().getString(string);
    }

}
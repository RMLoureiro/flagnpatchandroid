package pt.unl.fct.di.apdc.flagnpatch.Utils;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;

import java.util.List;

import pt.unl.fct.di.apdc.flagnpatch.ListActivity;
import pt.unl.fct.di.apdc.flagnpatch.R;
import pt.unl.fct.di.apdc.flagnpatch.Resources.Occurrence;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {

    public static final String TAG = "ListAdapter";
    private static final String NO_PHOTO = "/img/missing.png";
    private static final String DEFAULT_PHOTO = "https://itviec.com/assets/missing.png";


    private List<Occurrence> occurencesList;
    private AppSingleton appSingleton = AppSingleton.getInstance(new ListActivity());

    public ListAdapter(List<Occurrence> occurencesList) {
        this.occurencesList = occurencesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Occurrence currentOCcurence = occurencesList.get(position);
        if (currentOCcurence.getImageURL().equals(NO_PHOTO) ||
                currentOCcurence.getImageURL().equals(DEFAULT_PHOTO)) {
            //holder.mImageView.setImageResource(R.mipmap.missing);
        } else {
            this.getImageBitmap(currentOCcurence.getImageURL(), holder);
        }
        holder.mTitleView.setText(currentOCcurence.getAddress());
        holder.mTextView.setText(currentOCcurence.getType() + "\n" + currentOCcurence.getDescription());
        holder.mCreatedView.setText(currentOCcurence.getCreationDate().subSequence(0, 11));
        holder.mFollowedView.setText(currentOCcurence.getFollowers());
        holder.mStateView.setText(currentOCcurence.getState());
        switch (currentOCcurence.getState()) {
            case "Pendente":
                holder.mStateView.setTextColor(Color.parseColor("#757575"));
                break;
            case "Em Resolução":
                holder.mStateView.setTextColor(Color.parseColor("#FCF004"));
                break;
            case "Resolvido":
                holder.mStateView.setTextColor(Color.parseColor("#18BB08"));
                break;
            case "Rejeitado":
                holder.mStateView.setTextColor(Color.parseColor("#FF0000"));
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (occurencesList == null) {
            return -1;
        }
        return occurencesList.size();
    }

    private void getImageBitmap(String url, final MyViewHolder holder) {
        ImageRequest imageRequest = new ImageRequest(
                url, // Image URL
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        Bitmap thumbImage = ThumbnailUtils.extractThumbnail(
                                response, 64, 64);
                        holder.mImageView.setImageBitmap(thumbImage);
                    }
                },
                0, // Image width
                0, // Image height
                ImageView.ScaleType.CENTER_CROP, // Image scale type
                Bitmap.Config.RGB_565, //Image decode configuration
                new Response.ErrorListener() { // Error listener
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Do something with error response
                        if (error == null || error.networkResponse == null) {
                            return;
                        }
                    }
                }
        );
        appSingleton.addToRequestQueue(imageRequest, TAG);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView;
        public TextView mTitleView, mTextView, mCreatedView, mFollowedView, mStateView;

        public MyViewHolder(View listItemView) {
            super(listItemView);
            mImageView = (ImageView) listItemView.findViewById(R.id.imgIcon);
            mTitleView = (TextView) listItemView.findViewById(R.id.txtTitle);
            mTextView = (TextView) listItemView.findViewById(R.id.txtAddress);
            mCreatedView = (TextView) listItemView.findViewById(R.id.txtReported);
            mFollowedView = (TextView) listItemView.findViewById(R.id.txtVotes);
            mStateView = (TextView) listItemView.findViewById(R.id.txtState);
        }
    }
}
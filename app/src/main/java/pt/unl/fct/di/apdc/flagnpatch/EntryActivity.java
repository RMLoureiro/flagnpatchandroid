package pt.unl.fct.di.apdc.flagnpatch;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import pt.unl.fct.di.apdc.flagnpatch.Database.DBHandler;

public class EntryActivity extends Activity {
    private long ms = 0;
    private long entryTime = 2000;
    private boolean entryActive = true;
    private boolean paused = false;
    private DBHandler datastore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        datastore = new DBHandler(this);
        setContentView(R.layout.activity_entry);

        new Thread() {
            public void run() {
                try {
                    while (entryActive && ms < entryTime) {
                        if (!paused)
                            ms = ms + 100;
                        sleep(100);
                    }
                } catch (Exception e) {
                } finally {

                    Context ctx = getApplicationContext();
                    if (datastore.getToken() != null
                            && Long.valueOf(datastore.getToken().getExpirationDate()) - System.currentTimeMillis() > 0) {
                        startActivity(new Intent(ctx, MainActivity.class));
                    } else {
                        startActivity(new Intent(ctx, LoginActivity.class));
                    }
                }
            }
        }.start();


    }
}
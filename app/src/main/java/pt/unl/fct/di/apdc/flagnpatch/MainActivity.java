package pt.unl.fct.di.apdc.flagnpatch;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONException;
import org.json.JSONObject;

import pt.unl.fct.di.apdc.flagnpatch.Database.DBHandler;
import pt.unl.fct.di.apdc.flagnpatch.Resources.General.TokenData;
import pt.unl.fct.di.apdc.flagnpatch.Utils.AppSingleton;

public class MainActivity extends TabActivity {

    private static final String TAG = "MainActivity";
    private DBHandler datastore;
    private JSONObject tokenObj;
    private AppSingleton appSingleton;
/*
    private List<Occurrence> myReports;
    private List<Occurrence> allReports;
*/

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        datastore = new DBHandler(this);
        appSingleton = AppSingleton.getInstance(this);
        tokenObj = new JSONObject();

        TokenData token = null;

        if (datastore.getToken() != null) {
            token = datastore.getToken();
            try {
                tokenObj.accumulate(string(R.string.t_user), token.getUser());
                tokenObj.accumulate(string(R.string.t_id), token.getId());
                tokenObj.accumulate(string(R.string.t_creation), token.getCreationDate());
                tokenObj.accumulate(string(R.string.t_expiration), token.getExpirationDate());
                tokenObj.accumulate(string(R.string.t_role), token.getRole());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else tokenLost();


        Resources res = getResources();
        final TabHost tabHost = getTabHost();
        tabHost.setup();
        Bundle args;

        Intent intentMaps;
        if (token.getRole().equals(string(R.string.w_work)))
            intentMaps = new Intent().setClass(this, MapsActivityWorker.class);
        else
            intentMaps = new Intent().setClass(this, MapsActivity.class);

        TabSpec spec1 = tabHost.newTabSpec(string(R.string.tab_map))
                .setIndicator("", res.getDrawable(R.drawable.ic_map_white_24px))
                .setContent(intentMaps);

        Intent intentList;
        if (token.getRole().equals(string(R.string.w_work)))
            intentList = new Intent().setClass(this, ListWorkerActivity.class);
        else
            intentList = new Intent().setClass(this, ListActivity.class);

        TabSpec spec2 = tabHost.newTabSpec(string(R.string.tab_occ))
                .setIndicator("", res.getDrawable(R.drawable.ic_view_list_white_24px))
                .setContent(intentList);

        Intent intentFilters = new Intent().setClass(this, FiltersActivity.class);
        TabSpec spec3 = tabHost.newTabSpec(string(R.string.tab_filters))
                .setIndicator("", res.getDrawable(R.drawable.ic_filter_list_white_24px))
                .setContent(intentFilters);

        TabSpec spec4 = tabHost.newTabSpec(string(R.string.tab_logout))
                .setIndicator("", res.getDrawable(R.drawable.ic_power_settings_new_white_24px))
                .setContent(R.id.btn_logout);

        tabHost.addTab(spec1);
        tabHost.addTab(spec2);
        tabHost.addTab(spec3);
        tabHost.addTab(spec4);

        tabHost.setCurrentTab(0);

        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                switch (tabHost.getCurrentTab()) {
                    case 0:
                        findViewById(R.id.searchLocation).setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        findViewById(R.id.searchLocation).setVisibility(View.GONE);
                        break;
                    case 2:
                        findViewById(R.id.searchLocation).setVisibility(View.GONE);
                        break;
                    case 3:
                        JsonObjectRequest logout = new JsonObjectRequest(
                                Request.Method.POST, string(R.string.ser_logout), tokenObj,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                        DBHandler datastore = new DBHandler(MainActivity.this);
                                        datastore.deleteTable();
                                        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        if (error == null || error.networkResponse == null) {
                                            return;
                                        }
                                    }
                                });
                        appSingleton.addToRequestQueue(logout, TAG);

                }
                if (tabHost.getCurrentTab() == 1 || tabHost.getCurrentTab() == 2)
                    findViewById(R.id.searchLocation).setVisibility(View.GONE);
                if (tabHost.getCurrentTab() == 3) {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                    DBHandler datastore = new DBHandler(MainActivity.this);
                    datastore.deleteTable();
                    overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
                }
            }
        });

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.searchLocation);
        ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setTextColor(getResources().getColor(R.color.white));
        autocompleteFragment.setBoundsBias(new LatLngBounds(
                new LatLng(38.54554, -9.26284),
                new LatLng(38.69174, -9.12757)));


        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                if (place != null) {
                    LatLng location = place.getLatLng();
                    if (location != null && !location.equals("")) {
                        MapsActivity.mMap.moveCamera(CameraUpdateFactory.zoomTo(16));
                        MapsActivity.mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
                    }
                }
            }

            @Override
            public void onError(Status status) {
                System.out.println("AQUI: " + status.getStatusCode());
            }
        });

    }

    private void tokenLost() {
        Toast.makeText(this, string(R.string.e_expired_session), Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
        datastore.deleteTable();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    private String string(int string) {
        return this.getResources().getString(string);
    }

}
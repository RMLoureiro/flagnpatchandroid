package pt.unl.fct.di.apdc.flagnpatch;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pt.unl.fct.di.apdc.flagnpatch.Database.DBHandler;
import pt.unl.fct.di.apdc.flagnpatch.Resources.General.TokenData;
import pt.unl.fct.di.apdc.flagnpatch.Resources.Occurrence;
import pt.unl.fct.di.apdc.flagnpatch.Utils.AppSingleton;
import pt.unl.fct.di.apdc.flagnpatch.Utils.CustomJsonArrayRequest;
import pt.unl.fct.di.apdc.flagnpatch.Utils.PermissionUtils;

public class MapsActivity extends AppCompatActivity
        implements
        OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback {
    private static final String TAG = "MapsActivity";
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    /**
     * Request code for location permission request.
     *
     * @see #onRequestPermissionsResult(int, String[], int[])
     */
    static GoogleMap mMap;
    private final Marker[] tempMarker = new Marker[1];
    private boolean hasMarker;
    private Geocoder geocoder;
    private List<Occurrence> reports;
    private AppSingleton appSingleton;
    private DBHandler datastore;
    private JSONObject tokenObj;

    private Boolean exit = false;

    /**
     * Flag indicating whether a requested permission has been denied after returning in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     */
    private boolean mPermissionDenied = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        geocoder = new Geocoder(this);
        appSingleton = AppSingleton.getInstance(this);
        tokenObj = new JSONObject();
        datastore = new DBHandler(this);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (datastore.getToken() != null) {
            TokenData token = datastore.getToken();
            try {
                tokenObj.accumulate(string(R.string.t_user), token.getUser());
                tokenObj.accumulate(string(R.string.t_id), token.getId());
                tokenObj.accumulate(string(R.string.t_creation), token.getCreationDate());
                tokenObj.accumulate(string(R.string.t_expiration), token.getExpirationDate());
                tokenObj.accumulate(string(R.string.t_role), token.getRole());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else tokenLost();

        getAreas();

        MapFragment mapFragment = (MapFragment) this.getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        hasMarker = false;


        final FloatingActionButton button = (FloatingActionButton) findViewById(R.id.addReport);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!hasMarker) {
                    emptyAddMarker(null);
                } else {
                    hasMarker = false;

                    if(!tempMarker[0].getTitle().equals(string(R.string.map_new_occ))) {
                        LatLng ll = tempMarker[0].getPosition();
                        List<Address> geo = null;
                        try {
                            geo = geocoder.getFromLocation(ll.latitude, ll.longitude, 2);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        if(appSingleton.areas.contains(geo.get(1).getSubAdminArea())) {
                            Intent intent = new Intent(getApplicationContext(), NewOccurence.class);
                            Bundle args = new Bundle();
                            args.putParcelable(string(R.string.j_address), tempMarker[0].getPosition());
                            intent.putExtra(string(R.string.bundle), args);
                            startActivity(intent);
                            tempMarker[0].remove();
                        }
                        else
                            Toast.makeText(MapsActivity.this, "Área indisponível.", Toast.LENGTH_SHORT).show();
                    }
                    else
                        Toast.makeText(MapsActivity.this, "Área indisponível.", Toast.LENGTH_SHORT).show();



                }

            }

        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mMap != null)
            mMap.clear();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (appSingleton.myReports.isEmpty() || !appSingleton.myReports.equals(reports)) {
            getMyReports();
        } else {
            reports = appSingleton.myReports;
            setAllMarkers();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (appSingleton.myReports.isEmpty() || !appSingleton.myReports.equals(reports)) {
            getMyReports();
        } else {
            reports = appSingleton.myReports;
            setAllMarkers();
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (appSingleton.myReports.isEmpty() || !appSingleton.myReports.equals(reports)) {
            mMap.clear();
            getMyReports();
        } else
            reports = appSingleton.myReports;

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {

            @Override
            public void onMapLongClick(LatLng point) {
                emptyAddMarker(point);
            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                LatLng latLng = new LatLng(point.latitude, point.longitude);
                mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
            }
        });

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                Address address = null;
                LatLng latLng = null;
                try {
                    latLng = tempMarker[0].getPosition();
                    tempMarker[0].remove();
                    List<Address> temp = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    address = temp.get(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (address != null) {
                    String addr = "";
                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                        addr += address.getAddressLine(i) + " ";
                    }
                    tempMarker[0] = mMap.addMarker(new MarkerOptions().position(latLng).title(addr).draggable(true));
                    hasMarker = true;
                } else {
                    tempMarker[0] = mMap.addMarker(new MarkerOptions().position(latLng).title(string(R.string.map_new_occ)).draggable(true));
                    hasMarker = true;
                }
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker.getTag() != null){
                    goToSpecificOccurence((Occurrence) marker.getTag());
                }
                else
                    marker.showInfoWindow();
                return true;
            }
        });
        LatLng almada = new LatLng(38.6359837, -9.1874799);
        mMap.moveCamera(CameraUpdateFactory.zoomTo(12));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(almada));
        enableMyLocation();
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (mMap != null) {
            // Access to the location has been granted to the app.
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }


    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, string(R.string.back_again),
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    private void emptyAddMarker(LatLng point) {
        if (tempMarker[0] != null) {
            tempMarker[0].remove();
        }
        Address address = null;
        LatLng latLng;
        if (point != null)
            latLng = point;
        else
            latLng = mMap.getCameraPosition().target;
        try {
            List<Address> temp = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if(temp.isEmpty()) {
                tempMarker[0] = mMap.addMarker(new MarkerOptions().position(latLng).title(string(R.string.map_new_occ)).draggable(true));
                hasMarker = true;
                return;
            }
            address = temp.get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (address != null) {
            String addr = "";
            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                addr += address.getAddressLine(i) + " ";
            }
            tempMarker[0] = mMap.addMarker(new MarkerOptions().position(latLng).title(addr).draggable(true));
            hasMarker = true;
        } else {
            tempMarker[0] = mMap.addMarker(new MarkerOptions().position(latLng).title(string(R.string.map_new_occ)).draggable(true));
            hasMarker = true;
        }
    }

    public void setAllMarkers() {
        boolean pendente = appSingleton.pendente;
        boolean resolucao = appSingleton.resolucao;
        boolean resolvido = appSingleton.resolvido;
        boolean rejeitado = appSingleton.rejeitado;

        List<Occurrence> allMarkers = reports;
        if (allMarkers.isEmpty()) {
            return;
        }
        for (Occurrence o : allMarkers) {
            if ((o.getState().equals(string(R.string.st_pendente)) && pendente) ||
                    (o.getState().equals(string(R.string.st_em_resolucao)) && resolucao) ||
                    (o.getState().equals(string(R.string.st_resolvido)) && resolvido) ||
                    (o.getState().equals(string(R.string.st_rejeitado)) && rejeitado)) {
                int color = -1;
                switch (o.getState()) {
                    case "Pendente":
                        color = R.mipmap.ic_marker_grey;
                        break;
                    case "Em Resolução":
                        color = R.mipmap.ic_marker_yellow;
                        break;
                    case "Resolvido":
                        color = R.mipmap.ic_marker_green;
                        break;
                    case "Rejeitado":
                        color = R.mipmap.ic_marker_red;
                        break;
                }
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(o.getLatitude(), o.getLongitude()))
                        .icon(BitmapDescriptorFactory.fromResource(color))).setTag(o);
            }
        }
    }

    private void goToSpecificOccurence(Occurrence occ) {
        Intent intent = new Intent(this, OccDetailsUser.class);
        Bundle args = new Bundle();
        args.putParcelable(string(R.string.o_occ), occ);
        intent.putExtra(string(R.string.bundle), args);
        startActivity(intent);
    }

    public void getMyReports() {
        JsonRequest req = new CustomJsonArrayRequest(Request.Method.POST, string(R.string.ser_my_reports), tokenObj, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (response.length() != 0) {
                    reports = new ArrayList<>();
                    for (int i = 0; i < response.length(); i++) {
                        try {
                            JSONObject obj = response.getJSONObject(i);
                            JSONObject addr = obj.getJSONObject(string(R.string.j_address));
                            Occurrence occ = new Occurrence(
                                    obj.getString(string(R.string.j_type)),
                                    obj.getString(string(R.string.j_description)),
                                    obj.getString(string(R.string.j_imageUrl)),
                                    obj.getString(string(R.string.j_addressAsStreet)),
                                    addr.getString(string(R.string.j_district)),
                                    addr.getString(string(R.string.j_county)),
                                    obj.getString(string(R.string.t_id)),
                                    obj.getInt(string(R.string.j_priority)),
                                    obj.getString(string(R.string.j_state)),
                                    obj.getDouble(string(R.string.j_latitude)),
                                    obj.getDouble(string(R.string.j_longitude)),
                                    obj.getString(string(R.string.j_creationDate)),
                                    obj.getString(string(R.string.j_followers)),
                                    obj.getBoolean(string(R.string.j_isFollowing)));
                            reports.add(occ);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    appSingleton.myReports = reports;
                }
                if (reports.size() > 0)
                    setAllMarkers();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error == null || error.networkResponse == null) {
                    return;
                }
                if (error.networkResponse.statusCode == 401)
                    tokenLost();
                if (reports == null)
                    return;
            }
        });
        appSingleton.addToRequestQueue(req, TAG);

    }

    private void getAreas() {
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, string(R.string.ser_get_areas), tokenObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response.length() != 0) {
                    appSingleton.areas = new ArrayList<>();
                    try {
                        JSONArray obj = response.getJSONArray("counties");
                        for (int i = 0; i < obj.length(); i++) {
                            appSingleton.areas.add(obj.getString(i));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error == null || error.networkResponse == null) {
                    return;
                }
                if (error.networkResponse.statusCode == 401)
                    tokenLost();
            }
        });
        appSingleton.addToRequestQueue(req, TAG);
    }


    private void tokenLost() {
        Toast.makeText(this, string(R.string.e_expired_session), Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
        datastore.deleteTable();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    private String string(int string) {
        return this.getResources().getString(string);
    }

}
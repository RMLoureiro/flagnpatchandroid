package pt.unl.fct.di.apdc.flagnpatch.Resources;


public class OccComments {
    public final String content, authorEmail, authorName, date;
    public final int authorId;
    public final long registerTime;

    public OccComments(String content, String authorEmail, String authorName, int authorId, long registerTime, String date) {
        this.content = content;
        this.authorEmail = authorEmail;
        this.authorName = authorName;
        this.authorId = authorId;
        this.registerTime = registerTime;
        this.date = date;
    }
}

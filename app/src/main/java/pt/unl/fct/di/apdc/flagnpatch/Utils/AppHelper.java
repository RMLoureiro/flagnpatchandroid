package pt.unl.fct.di.apdc.flagnpatch.Utils;

import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class AppHelper {

    public static int doPOST(URL url, JSONObject data) throws IOException {

        InputStream stream = null;
        OutputStream out = null;
        HttpURLConnection connection = null;
        int responseCode = -1;
        try {
            connection = (HttpURLConnection) url.openConnection();
            // Timeout for reading InputStream arbitrarily set to 10000ms.
            connection.setReadTimeout(10000);
            // Timeout for connection.connect() arbitrarily set to 10000ms.
            connection.setConnectTimeout(10000);
            // For this use case, set HTTP method to GET.
            connection.setRequestMethod("POST");
            // Already true by default but setting just in case; needs to be true since this request
            // is carrying an input (response) body.
            connection.setDoInput(true);
            connection.setChunkedStreamingMode(0);
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-type", "image/jpeg");
            // Open communications link (network traffic occurs here).
            out = new BufferedOutputStream(connection.getOutputStream());
            String image = data.getString("image");
            byte[] byteArray = Base64.decode(image, Base64.DEFAULT);
            out.write(byteArray);
            out.flush();
            responseCode = connection.getResponseCode();

            return responseCode;
            // Retrieve the response body as an InputStream.
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            // Close streams and disconnect HTTP connection.
            if (out != null) out.close();
            if (stream != null) stream.close();
            if (connection != null) connection.disconnect();
        }
        return responseCode;
    }

}
// Generated code from Butter Knife. Do not modify!
package pt.unl.fct.di.apdc.flagnpatch;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class SignupActivity$$ViewBinder<T extends pt.unl.fct.di.apdc.flagnpatch.SignupActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755245, "field '_nameText'");
    target._nameText = finder.castView(view, 2131755245, "field '_nameText'");
    view = finder.findRequiredView(source, 2131755246, "field '_lastnameText'");
    target._lastnameText = finder.castView(view, 2131755246, "field '_lastnameText'");
    view = finder.findRequiredView(source, 2131755224, "field '_emailText'");
    target._emailText = finder.castView(view, 2131755224, "field '_emailText'");
    view = finder.findRequiredView(source, 2131755247, "field '_districtText'");
    target._districtText = finder.castView(view, 2131755247, "field '_districtText'");
    view = finder.findRequiredView(source, 2131755248, "field '_countyText'");
    target._countyText = finder.castView(view, 2131755248, "field '_countyText'");
    view = finder.findRequiredView(source, 2131755225, "field '_passwordText'");
    target._passwordText = finder.castView(view, 2131755225, "field '_passwordText'");
    view = finder.findRequiredView(source, 2131755249, "field '_reEnterPasswordText'");
    target._reEnterPasswordText = finder.castView(view, 2131755249, "field '_reEnterPasswordText'");
    view = finder.findRequiredView(source, 2131755250, "field '_signupButton'");
    target._signupButton = finder.castView(view, 2131755250, "field '_signupButton'");
    view = finder.findRequiredView(source, 2131755251, "field '_loginLink'");
    target._loginLink = finder.castView(view, 2131755251, "field '_loginLink'");
  }

  @Override public void unbind(T target) {
    target._nameText = null;
    target._lastnameText = null;
    target._emailText = null;
    target._districtText = null;
    target._countyText = null;
    target._passwordText = null;
    target._reEnterPasswordText = null;
    target._signupButton = null;
    target._loginLink = null;
  }
}

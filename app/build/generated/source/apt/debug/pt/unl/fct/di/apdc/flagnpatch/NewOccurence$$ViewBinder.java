// Generated code from Butter Knife. Do not modify!
package pt.unl.fct.di.apdc.flagnpatch;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class NewOccurence$$ViewBinder<T extends pt.unl.fct.di.apdc.flagnpatch.NewOccurence> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131755237, "field '_photoButton'");
    target._photoButton = finder.castView(view, 2131755237, "field '_photoButton'");
    view = finder.findRequiredView(source, 2131755238, "field '_typeText'");
    target._typeText = finder.castView(view, 2131755238, "field '_typeText'");
    view = finder.findRequiredView(source, 2131755242, "field '_priorityText'");
    target._priorityText = finder.castView(view, 2131755242, "field '_priorityText'");
    view = finder.findRequiredView(source, 2131755241, "field '_descriptionText'");
    target._descriptionText = finder.castView(view, 2131755241, "field '_descriptionText'");
    view = finder.findRequiredView(source, 2131755243, "field '_addressText'");
    target._addressText = finder.castView(view, 2131755243, "field '_addressText'");
    view = finder.findRequiredView(source, 2131755244, "field '_createOccButton'");
    target._createOccButton = finder.castView(view, 2131755244, "field '_createOccButton'");
  }

  @Override public void unbind(T target) {
    target._photoButton = null;
    target._typeText = null;
    target._priorityText = null;
    target._descriptionText = null;
    target._addressText = null;
    target._createOccButton = null;
  }
}
